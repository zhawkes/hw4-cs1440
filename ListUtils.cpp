#include <iostream>
#include <fstream>

#include "ListUtils.hpp"
#include <fstream>
#include <iostream>
Employment* build_empl_list(std::string filename) {
    std::ifstream dbFile;
    dbFile.open(filename);
    std::string junk;
    dbFile >> junk >> junk >> junk;
    Employment* retHead = NULL;
    Employment* tail = NULL;
    std::string area_fips;
    unsigned annual_avg_emplvl;
    unsigned long total_annual_wages;
    while(dbFile >> area_fips >> annual_avg_emplvl >> total_annual_wages){
        Employment* loop = new Employment(area_fips, annual_avg_emplvl, total_annual_wages);
        if(retHead == NULL){
            retHead = loop;
            tail = retHead;
        } else {
            tail->next = loop;
            tail = tail->next;
        }
    }
    
    dbFile.close();
    return retHead;
}


void append_lists(Employment* head, Employment* tail) {
    while (head->next)
        head = head->next;
    head->next = tail;
}


int list_length(Employment *emp) {
    if(emp == NULL) return 0;
    return list_length(emp->next) + 1;
}


void print_every_empl(Employment *emp) {
    if(emp == NULL) return;
    std::cout << emp << std::endl;
    print_every_empl(emp->next);
}

void cleanup_list(Employment* head){
    //void for now
    if(head == NULL) return;
    Employment* temp = head->next;
    delete head;
    head = NULL;
    cleanup_list(temp);
}
