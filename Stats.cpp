// #include <limits.h> // provides UINT_MAX, ULONG_MAX

#include "Stats.hpp"
#include "ListUtils.hpp"

#include <iostream>

unsigned long total_annual_wages(Employment *emp) {
    if(emp == NULL){
        return 0;
    }
    return emp->total_annual_wages + total_annual_wages(emp->next);
}


unsigned long min_annual_wages(Employment *emp) {
    if(emp==NULL)return 0;
    if(emp->next == NULL){
        return emp->total_annual_wages;
    }
    return min_annual_wages(emp->next);
}


// Assume that the list has been sorted appropriately
unsigned long med_annual_wages(Employment *emp) {
    int len = list_length(emp);
    int location;
    Employment* node = emp;
    if(len % 2 == 0){
        location = len / 2;
    } else {
        location = (len / 2) + 1;
    }
    for(int i = 0; i <= location; i++){
        if(node != NULL) 
            node = node->next;
        if(i == location){
            return node->total_annual_wages;
        }
    }
    return node->total_annual_wages;
}   


unsigned long max_annual_wages(Employment *emp) {
    if(emp == NULL) return 0;
    return emp->total_annual_wages;
}


unsigned total_annual_emplvl(Employment *emp) {
    if(emp == NULL){
        return 0;
    }
    return emp->annual_avg_emplvl + total_annual_emplvl(emp->next);
}


unsigned min_annual_emplvl(Employment *emp) {
    if(emp==NULL)return 0;
    if(emp->next == NULL){
        return emp->annual_avg_emplvl;
    }
    return min_annual_emplvl(emp->next);
}


// Assume that the list has been sorted appropriately
unsigned med_annual_emplvl(Employment *emp) {
    int len = list_length(emp);
    int location;
    Employment* node = emp;
    if(len % 2 == 0){
        location = len / 2;
    } else {
        location = (len / 2) + 1;
    }
    for(int i = 0; i <= location; i++){
        if(node != NULL) 
            node = node->next;
        if(i == location){
            return node->annual_avg_emplvl;
        }
    }
    return node->annual_avg_emplvl;
}


unsigned max_annual_emplvl(Employment *emp) {
    if(emp == NULL) return 0;
    return emp->annual_avg_emplvl;
}
